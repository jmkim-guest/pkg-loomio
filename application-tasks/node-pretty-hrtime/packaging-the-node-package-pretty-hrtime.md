# Packaging the Node package: pretty-hrtime

`npm2deb` is package template maker tool for Node programmes and libraries.

## 1. npm2deb 


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime $</b></font> npm2deb create pretty-hrtime

Downloading source tarball file using debian/watch file...
uscan: Newest version of node-pretty-hrtime on remote site is 1.0.3, specified download version is 1.0.3
Successfully symlinked ../node-pretty-hrtime-1.0.3.tar.gz to ../node-pretty-hrtime_1.0.3.orig.tar.gz.

Creating debian source package...
uupdate: debian/source/format is &quot;3.0 (quilt)&quot;.
uupdate: Auto-generating node-pretty-hrtime_1.0.3-1.debian.tar.xz
uupdate: -&gt; Use existing node-pretty-hrtime_1.0.3-1.debian.tar.xz
<b>dpkg-source: </b><font color="#859900">info</font>: extracting node-pretty-hrtime in node-pretty-hrtime-1.0.3
<b>dpkg-source: </b><font color="#859900">info</font>: unpacking node-pretty-hrtime_1.0.3.orig.tar.gz
<b>dpkg-source: </b><font color="#859900">info</font>: unpacking node-pretty-hrtime_1.0.3-1.debian.tar.xz
uupdate: Remember: Your current directory is changed back to the old source tree!
uupdate: Do a &quot;cd ../node-pretty-hrtime-1.0.3&quot; to see the new source tree and
    edit it to be nice Debianized source.

Building the binary package
<b>dpkg-source: </b><font color="#859900">info</font>: using source format &apos;3.0 (quilt)&apos;
<b>dpkg-source: </b><font color="#859900">info</font>: building node-pretty-hrtime using existing ./node-pretty-hrtime_1.0.3.orig.tar.gz
<b>dpkg-source: </b><font color="#859900">info</font>: building node-pretty-hrtime in node-pretty-hrtime_1.0.3-1.debian.tar.xz
<b>dpkg-source: </b><font color="#859900">info</font>: building node-pretty-hrtime in node-pretty-hrtime_1.0.3-1.dsc
<b>dpkg-buildpackage: </b><font color="#859900">info</font>: source package node-pretty-hrtime
<b>dpkg-buildpackage: </b><font color="#859900">info</font>: source version 1.0.3-1
<b>dpkg-buildpackage: </b><font color="#859900">info</font>: source distribution UNRELEASED
<b>dpkg-buildpackage: </b><font color="#859900">info</font>: source changed by Jongmin Kim &lt;jmkim@pukyong.ac.kr&gt;
<b>dpkg-buildpackage: </b><font color="#859900">info</font>: host architecture amd64
<font color="#6C71C4"><b> dpkg-source --before-build .</b></font>
<font color="#6C71C4"><b> fakeroot debian/rules clean</b></font>
dh clean
   dh_clean
<font color="#6C71C4"><b> dpkg-source -b .</b></font>
<b>dpkg-source: </b><font color="#859900">info</font>: using source format &apos;3.0 (quilt)&apos;
<b>dpkg-source: </b><font color="#859900">info</font>: building node-pretty-hrtime using existing ./node-pretty-hrtime_1.0.3.orig.tar.gz
<b>dpkg-source: </b><font color="#859900">info</font>: building node-pretty-hrtime in node-pretty-hrtime_1.0.3-1.debian.tar.xz
<b>dpkg-source: </b><font color="#859900">info</font>: building node-pretty-hrtime in node-pretty-hrtime_1.0.3-1.dsc
<font color="#6C71C4"><b> debian/rules build</b></font>
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
<font color="#6C71C4"><b> fakeroot debian/rules binary</b></font>
dh binary
   dh_testroot
   dh_prep
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
<b>dpkg-deb:</b> building package &apos;node-pretty-hrtime&apos; in &apos;../node-pretty-hrtime_1.0.3-1_all.deb&apos;.
<font color="#6C71C4"><b> dpkg-genbuildinfo</b></font>
<font color="#6C71C4"><b> dpkg-genchanges  &gt;../node-pretty-hrtime_1.0.3-1_amd64.changes</b></font>
<b>dpkg-genchanges: </b><font color="#859900">info</font>: including full source code in upload
<font color="#6C71C4"><b> dpkg-source --after-build .</b></font>
<b>dpkg-buildpackage: </b><font color="#859900">info</font>: full upload (original source is included)
<b>dpkg-buildpackage: </b><font color="#657B83"><b>warning</b></font>: not signing UNRELEASED build; use --force-sign to override
dh clean
   dh_clean

Remember, your new source directory is pretty-hrtime/node-pretty-hrtime-1.0.3

This is not a crystal ball, so please take a look at auto-generated files.

You may want fix first these issues:

<font color="#D33682">pretty-hrtime/node-pretty-hrtime-1.0.3/debian/control</font><font color="#2AA198">:</font>Description: <font color="#CB4B16"><b>FIX_ME</b></font> write the Debian package description
<font color="#D33682">pretty-hrtime/node-pretty-hrtime_itp.mail</font><font color="#2AA198">:</font>Subject: ITP: node-pretty-hrtime -- <font color="#CB4B16"><b>FIX_ME</b></font> write the Debian package description
<font color="#D33682">pretty-hrtime/node-pretty-hrtime_itp.mail</font><font color="#2AA198">:</font>  Description     : <font color="#CB4B16"><b>FIX_ME</b></font> write the Debian package description
<font color="#D33682">pretty-hrtime/node-pretty-hrtime_itp.mail</font><font color="#2AA198">:</font> <font color="#CB4B16"><b>FIX_ME</b></font>: This ITP report is not ready for submission, until you are
<font color="#D33682">pretty-hrtime/node-pretty-hrtime_itp.mail</font><font color="#2AA198">:</font><font color="#CB4B16"><b>FIX_ME</b></font>: Explain why this package is suitable for adding to Debian. Is
<font color="#D33682">pretty-hrtime/node-pretty-hrtime_itp.mail</font><font color="#2AA198">:</font><font color="#CB4B16"><b>FIX_ME</b></font>: Explain how you intend to consistently maintain this package</pre>


## 2. Files made by npm2deb

<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime $</b></font> cd pretty-hrtime/
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime $</b></font> ls -1
<font color="#268BD2">node-pretty-hrtime</font>
<font color="#268BD2">node-pretty-hrtime-1.0.3</font>
<font color="#6C71C4"><b>node-pretty-hrtime_1.0.3-1_all.deb</b></font>
node-pretty-hrtime_1.0.3-1_amd64.buildinfo
node-pretty-hrtime_1.0.3-1_amd64.changes
<font color="#6C71C4"><b>node-pretty-hrtime_1.0.3-1.debian.tar.xz</b></font>
node-pretty-hrtime_1.0.3-1.dsc
<font color="#D33682">node-pretty-hrtime_1.0.3.orig.tar.gz</font>
<font color="#6C71C4"><b>node-pretty-hrtime-1.0.3.tar.gz</b></font>
node-pretty-hrtime_itp.mail</pre>


  1. Directories:
      - `node-pretty-hrtime/`:
          Debian packaging work directory (debian directory)
      - `node-pretty-hrtime-1.0.3/`:
          Debian packaging work directory (upstream sources + debian directory)

  2. Packages:
      - `node-pretty-hrtime_1.0.3-1_all.deb`: Debian Binary package
      - `node-pretty-hrtime_1.0.3-1.debian.tar.xz`: Debian Source package

  3. Upstream sources:
      - `node-pretty-hrtime-1.0.3.tar.gz`: Upstream tarball
      - `node-pretty-hrtime_1.0.3.orig.tar.gz`:
          Symlink to upstream tarball (`node-pretty-hrtime-1.0.3.tar.gz`)

  4. Building informations:
      - `node-pretty-hrtime_1.0.3-1_amd64.buildinfo`:
          Building informations during packaging job

  5. Changes:
      - `node-pretty-hrtime_1.0.3-1.dsc`:
          `.dsc` file summarizes information in debian directory (just picks
          some important fields) and adds checksum for the tar files which
          will use to build the .deb file [1].
      - `node-pretty-hrtime_1.0.3-1_amd64.changes`:
          `.changes` is similar to `.dsc`, but has additional checksums for
          `.dsc` and `.deb` files too. [DDs (Debian Developer)](https://wiki.debian.org/DebianDeveloper)
          sign the changes file with their GPG key, so DDs know the checksums
          are not modified [1].

  6. Other files specially made by npm2deb:
      - `node-pretty-hrtime_itp.mail`:
          [ITP](https://wiki.debian.org/ITP) mail template


## 3. Convert source package to Git repository

<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime $</b></font> rm -r node-pretty-hrtime
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime $</b></font> gbp import-dsc --pristine-tar node-pretty-hrtime_1.0.3-1.dsc
<font color="#859900">gbp:info:</font> No git repository found, creating one.
<font color="#859900">gbp:info:</font> Version &apos;1.0.3-1&apos; imported under &apos;/home/jmkim/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime&apos;
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime $</b></font> cd node-pretty-hrtime
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git status
On branch master
nothing to commit, working tree clean</pre>


## 4. Push source package to Salsa


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git remote -v
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git remote add salsa-jmkim-guest git@salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git remote -v
salsa-jmkim-guest	git@salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git (fetch)
salsa-jmkim-guest	git@salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git (push)
</pre>


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git --no-pager branch
* <font color="#859900">master</font>
  pristine-tar
  upstream</pre>


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git checkout master
Switched to branch &apos;master&apos;</pre>


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git push salsa-jmkim-guest master
Enumerating objects: 31, done.
Counting objects: 100% (31/31), done.
Delta compression using up to 8 threads
Compressing objects: 100% (25/25), done.
Writing objects: 100% (31/31), 7.22 KiB | 3.61 MiB/s, done.
Total 31 (delta 4), reused 0 (delta 0)
To salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git
 * [new branch]      master -&gt; master</pre>


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git checkout upstream
Switched to branch &apos;upstream&apos;
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git push salsa-jmkim-guest upstream
Total 0 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for upstream, visit:
remote:   https://salsa.debian.org/jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime/merge_requests/new?merge_request%5Bsource_branch%5D=upstream
remote:
To salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git
 * [new branch]      upstream -&gt; upstream</pre>


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git checkout pristine-tar
Switched to branch &apos;pristine-tar&apos;
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git push salsa-jmkim-guest pristine-tar
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 1.68 KiB | 1.68 MiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for pristine-tar, visit:
remote:   https://salsa.debian.org/jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime/merge_requests/new?merge_request%5Bsource_branch%5D=pristine-tar
remote:
To salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git
 * [new branch]      pristine-tar -&gt; pristine-tar</pre>


<pre><font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git checkout master
Switched to branch &apos;master&apos;
<font color="#586E75"><b>jmkim@debian-20190215</b></font> <font color="#839496"><b>~/js-team/pretty-hrtime/pretty-hrtime/node-pretty-hrtime $</b></font> git push salsa-jmkim-guest master --tags
Enumerating objects: 2, done.
Counting objects: 100% (2/2), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 318 bytes | 318.00 KiB/s, done.
Total 2 (delta 0), reused 0 (delta 0)
To salsa.debian.org:jmkim-guest/pkg-loomio-application-tasks-node-pretty-hrtime.git
 * [new tag]         debian/1.0.3-1 -&gt; debian/1.0.3-1
 * [new tag]         upstream/1.0.3 -&gt; upstream/1.0.3</pre>


## References

[1] https://wiki.debian.org/Javascript/Nodejs/Npm2Deb/Tutorial
