# GSoC 2019: Package Loomio for Debian

A workspace for proposal of [**Package Loomio for Debian**](https://wiki.debian.org/SummerOfCode2019/Projects/PackageLoomioForDebian) project, by
Jongmin Kim <[jmkim@pukyong.ac.kr](mailto:jmkim@pukyong.ac.kr)>.


## Application tasks

- [x] Understand basics of packaging by reading Debian New maintainer's
      Guide: https://www.debian.org/doc/manuals/maint-guide/index.en.html

- [x] Study the various Ruby/Node packages within Debian.
    - Ruby Team: https://salsa.debian.org/ruby-team
    - Javascript Maintainers: https://salsa.debian.org/js-team

- [ ] Study the various packaging tools.
    - [x] gem2deb
        - [ ] ruby-team/meta
    - [x] npm2deb: https://wiki.debian.org/Javascript/Nodejs/Npm2Deb/Tutorial
    - [x] dh-make-golang
    - [x] dh_make

- [x] Package some beginner friendly Ruby/Node packages.
    - [x] pretty-hrtime
    - [ ] ruby-spoon

- [ ] Solve some bugs within Ruby/Javascript teams.
    - Ruby Team Bug Tracker:
      https://bugs.debian.org/cgi-bin/pkgreport.cgi?maint=pkg-ruby-extras-maintainers@lists.alioth.debian.org
        - Ruby Team: #debian-ruby on OFTC and
          [pkg-ruby-extras-maintainers@lists.alioth.debian.org](https://alioth-lists.debian.net/pipermail/pkg-ruby-extras-maintainers/)
    - Javascript Maintainers Bug Tracker:
      https://bugs.debian.org/cgi-bin/pkgreport.cgi?maint=pkg-javascript-devel@lists.alioth.debian.org
        - Javascript Maintainers: #debian-js on OFTC and
          [pkg-javascript-devel@lists.alioth.debian.org](https://alioth-lists.debian.net/pipermail/pkg-javascript-devel/)


## Proposal



## References

